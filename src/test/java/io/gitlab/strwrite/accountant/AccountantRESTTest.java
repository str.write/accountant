package io.gitlab.strwrite.accountant;

import io.gitlab.strwrite.accountant.model.Account;
import io.gitlab.strwrite.accountant.model.Accounts;
import io.gitlab.strwrite.accountant.model.requests.AccountCreateRequest;
import io.gitlab.strwrite.accountant.model.requests.TransferRequest;
import io.gitlab.strwrite.accountant.resources.AccountsREST;
import io.gitlab.strwrite.accountant.resources.TransfersREST;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

import static org.junit.Assert.*;

@Slf4j
public class AccountantRESTTest extends JerseyTest {

    @Override
    public Application configure() {
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new ResourceConfig(AccountsREST.class, TransfersREST.class);
    }

    @Test
    public void testBadOwnerName400() {
        Response accountCreated = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("", new BigInteger("0"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 400", 400, accountCreated.getStatus());
    }

    @Test
    public void testGetNotExist404() {
        Response accountGot = target("/accounts/" + UUID.randomUUID()).request().get();
        assertEquals("should return status 404", 404, accountGot.getStatus());
    }

    @Test
    public void testDeadlock() throws InterruptedException {
        log.info("Creating two accounts");

        Response accountCreated1 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName1", new BigInteger("20"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated1.getStatus());
        assertNotNull("Should return location", accountCreated1.getLocation());
        URI createdAccLocation1 = accountCreated1.getLocation();

        Response accountCreated2 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName2", new BigInteger("20"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated2.getStatus());
        assertNotNull("Should return location", accountCreated2.getLocation());
        URI createdAccLocation2 = accountCreated2.getLocation();

        Response accountGot1 = target(createdAccLocation1.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot1.getStatus());
        assertNotNull("Should return account", accountGot1.getEntity());
        Account account1 = accountGot1.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName1", account1.getOwnerName());
        assertNotNull("Id not null", account1.getId());
        assertEquals("Should have amount 20", new BigInteger("20"), account1.getAmount());

        Response accountGot2 = target(createdAccLocation2.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot2.getStatus());
        assertNotNull("Should return account", accountGot2.getEntity());
        Account account2 = accountGot2.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName2", account2.getOwnerName());
        assertNotNull("Id not null", account2.getId());
        assertEquals("Should have amount 20", new BigInteger("20"), account2.getAmount());

        log.info("Transferring");
        ExecutorService parallel = Executors.newFixedThreadPool(20);

        List<Callable<Integer>> todo = new ArrayList<>();


        for (int i = 0; i < 5; i++) {
            todo.add(() -> {
                Response transferred = target("/transfers/").request().post(Entity.entity(
                        new TransferRequest(account1.getId(), account2.getId(), new BigInteger("2"))
                        , MediaType.APPLICATION_JSON));
                assertEquals("should return status 201", 201, transferred.getStatus());
                return 1;
            });
        }

        for (int i = 0; i < 10; i++) {
            todo.add(() -> {
                Response transferred = target("/transfers/").request().post(Entity.entity(
                        new TransferRequest(account2.getId(), account1.getId(), new BigInteger("1"))
                        , MediaType.APPLICATION_JSON));
                assertEquals("should return status 201", 201, transferred.getStatus());
                return 1;
            });
        }

        List<Future<Integer>> futures = parallel.invokeAll(todo, 20, TimeUnit.SECONDS);
        parallel.shutdown();
        if (countSuccess(futures) < 15) {
            fail("Not all tasks are proceeded well, possibly there was a deadlock and timeout fired");
        }

        Response accountGot1after = target(createdAccLocation1.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot1after.getStatus());
        assertNotNull("Should return account", accountGot1after.getEntity());
        Account account1after = accountGot1after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName1", account1after.getOwnerName());
        assertNotNull("Id not null", account1after.getId());
        assertEquals("Should have amount 20", new BigInteger("20"), account1after.getAmount());

        Response accountGot2after = target(createdAccLocation2.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot2after.getStatus());
        assertNotNull("Should return account", accountGot2after.getEntity());
        Account account2after = accountGot2after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName2", account2after.getOwnerName());
        assertNotNull("Id not null", account2after.getId());
        assertEquals("Should have amount 20", new BigInteger("20"), account2after.getAmount());

    }


    @Test
    public void testPostAccountAndGetAccounts() {

        Response accountsGotBefore = target("/accounts/").request().get();
        assertEquals("should return status 200", 200, accountsGotBefore.getStatus());
        assertNotNull("Should return accounts", accountsGotBefore.getEntity());

        Accounts accountsBefore = accountsGotBefore.readEntity(Accounts.class);
        int before = accountsBefore.getAccounts().size();

        Response accountCreated = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("0"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated.getStatus());
        assertNotNull("Should return location", accountCreated.getLocation());

        Response accountCreated2 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName2", new BigInteger("0"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated2.getStatus());
        assertNotNull("Should return location", accountCreated2.getLocation());

        Response accountsGot = target("/accounts/").request().get();
        assertEquals("should return status 200", 200, accountsGot.getStatus());
        assertNotNull("Should return accounts", accountsGot.getEntity());

        Accounts accounts = accountsGot.readEntity(Accounts.class);
        if (accounts.getAccounts().size() - before != 2) {
            fail("Expected exactly two accs added, but got " + (accounts.getAccounts().size() - before));
        }
    }

    @Test
    public void testPostAndGetAccount() {
        Response accountCreated = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("0"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated.getStatus());
        assertNotNull("Should return location", accountCreated.getLocation());
        URI createdAccLocation = accountCreated.getLocation();

        Response accountGot = target(createdAccLocation.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot.getStatus());
        assertNotNull("Should return account", accountGot.getEntity());
        Account account = accountGot.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account.getOwnerName());
        assertNotNull("Id not null", account.getId());
    }

    @Test
    public void testTransferMoreThanHave() {
        log.info("Creating two accounts");

        Response accountCreated10 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("10"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated10.getStatus());
        assertNotNull("Should return location", accountCreated10.getLocation());
        URI createdAccLocation10 = accountCreated10.getLocation();

        Response accountCreated0 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("0"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated0.getStatus());
        assertNotNull("Should return location", accountCreated0.getLocation());
        URI createdAccLocation0 = accountCreated0.getLocation();

        Response accountGot10 = target(createdAccLocation10.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot10.getStatus());
        assertNotNull("Should return account", accountGot10.getEntity());
        Account account10 = accountGot10.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account10.getOwnerName());
        assertNotNull("Id not null", account10.getId());
        assertEquals("Should have amount 10", new BigInteger("10"), account10.getAmount());

        Response accountGot0 = target(createdAccLocation0.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot0.getStatus());
        assertNotNull("Should return account", accountGot0.getEntity());
        Account account0 = accountGot0.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account0.getOwnerName());
        assertNotNull("Id not null", account0.getId());
        assertEquals("Should have amount 0", new BigInteger("0"), account0.getAmount());

        log.info("Transfering");

        Response transferred = target("/transfers/").request().post(Entity.entity(
                new TransferRequest(account10.getId(), account0.getId(), new BigInteger("12"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 400", 400, transferred.getStatus());

        Response accountGot10after = target(createdAccLocation10.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot10after.getStatus());
        assertNotNull("Should return account", accountGot10after.getEntity());
        Account account10after = accountGot10after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account10after.getOwnerName());
        assertNotNull("Id not null", account10after.getId());
        assertEquals("Should have amount 10", new BigInteger("10"), account10after.getAmount());

        Response accountGot0after = target(createdAccLocation0.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot0after.getStatus());
        assertNotNull("Should return account", accountGot0after.getEntity());
        Account account0after = accountGot0after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account0after.getOwnerName());
        assertNotNull("Id not null", account0after.getId());
        assertEquals("Should have amount 0", new BigInteger("0"), account0after.getAmount());
    }


    @Test
    public void testTransfer() {
        log.info("Creating two accounts");

        Response accountCreated10 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("10"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated10.getStatus());
        assertNotNull("Should return location", accountCreated10.getLocation());
        URI createdAccLocation10 = accountCreated10.getLocation();

        Response accountCreated0 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("0"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated0.getStatus());
        assertNotNull("Should return location", accountCreated0.getLocation());
        URI createdAccLocation0 = accountCreated0.getLocation();

        Response accountGot10 = target(createdAccLocation10.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot10.getStatus());
        assertNotNull("Should return account", accountGot10.getEntity());
        Account account10 = accountGot10.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account10.getOwnerName());
        assertNotNull("Id not null", account10.getId());
        assertEquals("Should have amount 10", new BigInteger("10"), account10.getAmount());

        Response accountGot0 = target(createdAccLocation0.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot0.getStatus());
        assertNotNull("Should return account", accountGot0.getEntity());
        Account account0 = accountGot0.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account0.getOwnerName());
        assertNotNull("Id not null", account0.getId());
        assertEquals("Should have amount 0", new BigInteger("0"), account0.getAmount());

        log.info("Transferring");

        Response transferred = target("/transfers/").request().post(Entity.entity(
                new TransferRequest(account10.getId(), account0.getId(), new BigInteger("7"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, transferred.getStatus());

        Response accountGot10after = target(createdAccLocation10.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot10after.getStatus());
        assertNotNull("Should return account", accountGot10after.getEntity());
        Account account10after = accountGot10after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account10after.getOwnerName());
        assertNotNull("Id not null", account10after.getId());
        assertEquals("Should have amount 3", new BigInteger("3"), account10after.getAmount());

        Response accountGot0after = target(createdAccLocation0.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot0after.getStatus());
        assertNotNull("Should return account", accountGot0after.getEntity());
        Account account0after = accountGot0after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account0after.getOwnerName());
        assertNotNull("Id not null", account0after.getId());
        assertEquals("Should have amount 7", new BigInteger("7"), account0after.getAmount());
    }


    @Test
    public void testAFewParallelTransfers() throws InterruptedException {
        log.info("Creating two accounts");

        Response accountCreated10 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("10"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated10.getStatus());
        assertNotNull("Should return location", accountCreated10.getLocation());
        URI createdAccLocation10 = accountCreated10.getLocation();

        Response accountCreated0 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("testName", new BigInteger("0"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated0.getStatus());
        assertNotNull("Should return location", accountCreated0.getLocation());
        URI createdAccLocation0 = accountCreated0.getLocation();

        Response accountGot10 = target(createdAccLocation10.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot10.getStatus());
        assertNotNull("Should return account", accountGot10.getEntity());
        Account account10 = accountGot10.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account10.getOwnerName());
        assertNotNull("Id not null", account10.getId());
        assertEquals("Should have amount 10", new BigInteger("10"), account10.getAmount());

        Response accountGot0 = target(createdAccLocation0.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot0.getStatus());
        assertNotNull("Should return account", accountGot0.getEntity());
        Account account0 = accountGot0.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account0.getOwnerName());
        assertNotNull("Id not null", account0.getId());
        assertEquals("Should have amount 0", new BigInteger("0"), account0.getAmount());

        log.info("Transfering");
        ExecutorService parallel = Executors.newFixedThreadPool(7);

        List<Callable<Void>> todo = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            todo.add(() -> {
                Response transferred = target("/transfers/").request().post(Entity.entity(
                        new TransferRequest(account10.getId(), account0.getId(), new BigInteger("1"))
                        , MediaType.APPLICATION_JSON));
                assertEquals("should return status 201", 201, transferred.getStatus());
                return null;
            });
        }

        parallel.invokeAll(todo);
        parallel.shutdown();

        Response accountGot10after = target(createdAccLocation10.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot10after.getStatus());
        assertNotNull("Should return account", accountGot10after.getEntity());
        Account account10after = accountGot10after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account10after.getOwnerName());
        assertNotNull("Id not null", account10after.getId());
        assertEquals("Should have amount 3", new BigInteger("3"), account10after.getAmount());

        Response accountGot0after = target(createdAccLocation0.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot0after.getStatus());
        assertNotNull("Should return account", accountGot0after.getEntity());
        Account account0after = accountGot0after.readEntity(Account.class);
        assertEquals("Should return testName owner", "testName", account0after.getOwnerName());
        assertNotNull("Id not null", account0after.getId());
        assertEquals("Should have amount 7", new BigInteger("7"), account0after.getAmount());
    }


    @Test
    public void testTryDoubleSpend() throws InterruptedException {
        log.info("Creating accounts for A, B and C");

        Response accountCreated1 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("A", new BigInteger("20"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated1.getStatus());
        assertNotNull("Should return location", accountCreated1.getLocation());
        URI createdAccLocation1 = accountCreated1.getLocation();

        Response accountCreated2 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("B", new BigInteger("20"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated2.getStatus());
        assertNotNull("Should return location", accountCreated2.getLocation());
        URI createdAccLocation2 = accountCreated2.getLocation();

        Response accountCreated3 = target("/accounts/").request().post(Entity.entity(
                new AccountCreateRequest("C", new BigInteger("20"))
                , MediaType.APPLICATION_JSON));
        assertEquals("should return status 201", 201, accountCreated3.getStatus());
        assertNotNull("Should return location", accountCreated3.getLocation());
        URI createdAccLocation3 = accountCreated3.getLocation();

        Response accountGot1 = target(createdAccLocation1.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot1.getStatus());
        assertNotNull("Should return account", accountGot1.getEntity());
        Account account1 = accountGot1.readEntity(Account.class);
        assertEquals("Should return A owner", "A", account1.getOwnerName());
        assertNotNull("Id not null", account1.getId());
        assertEquals("Should have amount 20", new BigInteger("20"), account1.getAmount());

        Response accountGot2 = target(createdAccLocation2.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot2.getStatus());
        assertNotNull("Should return account", accountGot2.getEntity());
        Account account2 = accountGot2.readEntity(Account.class);
        assertEquals("Should return B owner", "B", account2.getOwnerName());
        assertNotNull("Id not null", account2.getId());
        assertEquals("Should have amount 20", new BigInteger("20"), account2.getAmount());

        Response accountGot3 = target(createdAccLocation3.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot3.getStatus());
        assertNotNull("Should return account", accountGot3.getEntity());
        Account account3 = accountGot3.readEntity(Account.class);
        assertEquals("Should return C owner", "C", account3.getOwnerName());
        assertNotNull("Id not null", account3.getId());
        assertEquals("Should have amount 20", new BigInteger("20"), account3.getAmount());

        log.info("Transferring from A to B and from A to C more than available");
        ExecutorService parallel = Executors.newFixedThreadPool(20);

        List<Callable<Integer>> todo = new ArrayList<>();

        prepareTransfers(account1, account2, todo, 10, "2");

        prepareTransfers(account1, account3, todo, 10, "2");

        List<Future<Integer>> futures = parallel.invokeAll(todo, 20, TimeUnit.SECONDS);
        parallel.shutdown();

        int testsum = countSuccess(futures);

        if (testsum < 10) {
            fail("Not all tasks are proceeded well, possibly there was a deadlock and timeout fired");
        }

        if (testsum > 10) {
            fail("Too much tasks successful");
        }

        Response accountGot1after = target(createdAccLocation1.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot1after.getStatus());
        assertNotNull("Should return account", accountGot1after.getEntity());
        Account account1after = accountGot1after.readEntity(Account.class);
        assertEquals("Should return A owner", "A", account1after.getOwnerName());
        assertNotNull("Id not null", account1after.getId());
        assertEquals("Should have amount 0", new BigInteger("0"), account1after.getAmount());

        Response accountGot2after = target(createdAccLocation2.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot2after.getStatus());
        assertNotNull("Should return account", accountGot2after.getEntity());
        Account account2after = accountGot2after.readEntity(Account.class);
        assertEquals("Should return B owner", "B", account2after.getOwnerName());
        assertNotNull("Id not null", account2after.getId());
        BigInteger secondAmount = account2after.getAmount();

        Response accountGot3after = target(createdAccLocation3.getPath()).request().get();
        assertEquals("should return status 200", 200, accountGot3after.getStatus());
        assertNotNull("Should return account", accountGot3after.getEntity());
        Account account3after = accountGot3after.readEntity(Account.class);
        assertEquals("Should return C owner", "C", account3after.getOwnerName());
        assertNotNull("Id not null", account3after.getId());
        BigInteger thirdAmount = account3after.getAmount();
        assertEquals("Sum of second and third should be 60", new BigInteger("60"), thirdAmount.add(secondAmount));
    }

    private void prepareTransfers(Account account1,
                                  Account account3,
                                  List<Callable<Integer>> todo,
                                  int number,
                                  String amount) {
        for (int i = 0; i < number; i++) {
            todo.add(() -> {
                Response transferred = target("/transfers/").request().post(Entity.entity(
                        new TransferRequest(account1.getId(), account3.getId(), new BigInteger(amount))
                        , MediaType.APPLICATION_JSON));
                if (transferred.getStatus() == 201) {
                    return 1;
                } else {
                    return 0;
                }
            });
        }
    }

    private int countSuccess(List<Future<Integer>> futures) {
        return futures.stream().mapToInt(it -> {
            try {
                return it.get();
            } catch (CancellationException e) {
                log.error("One of tasks has been terminated on 20 seconds timeout");
                throw new RuntimeException("One of tasks has been cancelled, probably stuck in deadlock", e);
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException("Unknown error", e);
            }
        }).sum();
    }


}
