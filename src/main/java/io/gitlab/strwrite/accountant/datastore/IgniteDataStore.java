package io.gitlab.strwrite.accountant.datastore;

import io.gitlab.strwrite.accountant.model.Account;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.TransactionConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
public class IgniteDataStore {

    public static class SingletonHolder {
        static final IgniteDataStore HOLDER_INSTANCE = new IgniteDataStore();
    }

    public static IgniteDataStore getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    private Ignite ignite;
    @Getter
    private IgniteTransactions transactions;

    private IgniteCache<String, Account> accounts;

    private IgniteDataStore() {
        CacheConfiguration cacheCfg = new CacheConfiguration();
        cacheCfg.setName("accounts");
        cacheCfg.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setCacheConfiguration(cacheCfg);
        TransactionConfiguration txCfg = new TransactionConfiguration();
        cfg.setTransactionConfiguration(txCfg);

        ignite = Ignition.start(cfg);
        transactions = ignite.transactions();
        accounts = ignite.getOrCreateCache(
                "accounts");
    }

    public void storeNewAccount(Account account) {
        accounts.put(account.getId(), account);
    }

    public void updateAccount(Account oldVal, Account newVal) {
        if (!oldVal.getId().equals(newVal.getId())) {
            throw new IllegalArgumentException(
                    "Cannot update accounts on entities with different ids:" +
                            oldVal.getId() + " , " + newVal.getId());
        }
        accounts.replace(oldVal.getId(), oldVal, newVal);
    }

    public Optional<Account> getAccount(UUID id) {
        return Optional.ofNullable(accounts.get(id.toString()));
    }

    public List<Account> getAccounts() {
        List<Account> accountsTemp = new ArrayList<>();
        accounts.forEach(it -> accountsTemp.add(it.getValue()));
        return accountsTemp;
    }
}
