package io.gitlab.strwrite.accountant.resources;

import io.gitlab.strwrite.accountant.datastore.IgniteDataStore;
import io.gitlab.strwrite.accountant.model.Account;
import io.gitlab.strwrite.accountant.model.Transfer;
import io.gitlab.strwrite.accountant.model.requests.TransferRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionException;
import org.apache.ignite.transactions.TransactionIsolation;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Random;
import java.util.UUID;

import static javax.ws.rs.core.Response.Status.*;

@Slf4j
@Path("/transfers")
public class TransfersREST {
    private Random random = new Random();

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transfer(TransferRequest tr) {
        Transfer transfer = new Transfer(tr);

        int limit = 5;
        boolean success = false;
        int wait = 100 + random.nextInt(900);
        while (limit-- > 0) {
            try (Transaction tx = getDataStore().getTransactions()
                    .txStart(TransactionConcurrency.OPTIMISTIC, TransactionIsolation.SERIALIZABLE)) {
                Response failResponse = transferInTransaction(transfer);
                if (failResponse != null) return failResponse;
                tx.commit();
                success = true;
                break;
            } catch (TransactionException e) {
                log.error("Transfer conflict write for {}, retrying", transfer.getId(), e);
            }
            wait = wait * 2;
            try {
                Thread.sleep(wait);
            } catch (InterruptedException e) {
                log.error("Transfer retry interrupted: id: {}", transfer.getId());
                return conflict();
            }
        }

        if (!success) {
            log.error("Failed to transfer {} after {} tries", transfer.getId(), limit);
            return conflict();
        }

        return Response.status(CREATED).build();
    }

    private Response conflict() {
        return Response.status(CONFLICT)
                .entity("Conflicted with other transfer")
                .build();
    }

    private Response transferInTransaction(Transfer transfer) {
        Response failResponse = resolveAccounts(transfer);
        if (failResponse != null) return failResponse;

        Account toOnStart = new Account(transfer.getToAccount());
        Account fromOnStart = new Account(transfer.getFromAccount());

        freezeTransferAmount(transfer);
        sendTransferAmount(transfer);
        Account fromAfterFreezeRemove = prepareFreezeRemove(transfer);

        getDataStore().updateAccount(fromOnStart, transfer.getFromAccount());
        log.info("Transfer {} freezed", transfer.getId());

        getDataStore().updateAccount(toOnStart, transfer.getToAccount());
        log.info("Transfer {} received", transfer.getId());

        getDataStore().updateAccount(transfer.getFromAccount(), fromAfterFreezeRemove);
        log.info("Transfer {} freeze removed", transfer.getId());
        return null;
    }

    private Response resolveAccounts(Transfer transfer) {
        try {
            transfer.setFromAccount(getAccount(transfer.getFrom()));
            transfer.setToAccount(getAccount(transfer.getTo()));
        } catch (IllegalArgumentException e) {
            log.error("Incorrect ids received for transfer: from: {}, to: {}",
                    transfer.getFrom(), transfer.getTo(), e);
            return Response.status(NOT_FOUND).entity("Failed to find such accounts").build();
        }

        if (transfer.getFromAccount().getAmount().compareTo(transfer.getAmount()) < 0) {
            log.debug("Cannot spend more than account has, account: {}, transfer: {}",
                    transfer.getFromAccount().getId(), transfer.getId());
            return Response.status(BAD_REQUEST)
                    .entity("Not allowed to transfer more than account currently has")
                    .build();
        }
        return null;
    }

    private Account prepareFreezeRemove(Transfer transfer) {
        Account fromAfterFreezeRemove = new Account(transfer.getFromAccount());
        fromAfterFreezeRemove.setFreezed(fromAfterFreezeRemove.getFreezed().subtract(transfer.getAmount()));
        return fromAfterFreezeRemove;
    }

    private void sendTransferAmount(Transfer transfer) {
        transfer.getToAccount().setAmount(transfer.getToAccount().getAmount().add(transfer.getAmount()));
        transfer.getToAccount().getTransfers().add(transfer.getId());
    }

    private void freezeTransferAmount(Transfer transfer) {
        transfer.getFromAccount().setAmount(transfer.getFromAccount().getAmount().subtract(transfer.getAmount()));
        transfer.getFromAccount().setFreezed(transfer.getFromAccount().getFreezed().add(transfer.getAmount()));
        transfer.getFromAccount().getTransfers().add(transfer.getId());
    }

    private Account getAccount(String id) {
        UUID fromId = UUID.fromString(id);
        return getDataStore().getAccount(fromId)
                .orElseThrow(() -> new IllegalArgumentException("No such account: " + fromId));
    }

    private IgniteDataStore getDataStore() {
        return IgniteDataStore.getInstance();
    }


}
