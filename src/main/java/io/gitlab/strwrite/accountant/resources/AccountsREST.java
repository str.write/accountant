package io.gitlab.strwrite.accountant.resources;

import io.gitlab.strwrite.accountant.datastore.IgniteDataStore;
import io.gitlab.strwrite.accountant.model.Account;
import io.gitlab.strwrite.accountant.model.Accounts;
import io.gitlab.strwrite.accountant.model.requests.AccountCreateRequest;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.util.StringUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.Optional;
import java.util.UUID;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Path("")
@Slf4j
public class AccountsREST {


    private IgniteDataStore getDataStore() {
        return IgniteDataStore.getInstance();
    }


    @POST
    @Path("/accounts")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response newAccount(AccountCreateRequest acr, @Context UriInfo uriInfo) {
        if (StringUtil.isBlank(acr.getOwnerName())) {
            return Response
                    .status(BAD_REQUEST)
                    .entity("Cannot create account without owner name")
                    .build();
        }
        Account account = new Account(acr);
        getDataStore().storeNewAccount(account);
        return Response.created(
                UriBuilder.fromPath("accounts")
                        .path(account.getId())
                        .build()
        ).build();
    }

    @GET
    @Path("/accounts")
    @Produces(MediaType.APPLICATION_JSON)
    public Accounts getAccounts() {
        log.info("Get accs");
        return new Accounts(getDataStore().getAccounts());
    }

    @GET
    @Path("/accounts/{accountId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccount(@PathParam("accountId") UUID accountId) {
        log.info("Get request {}", accountId);
        Optional<Account> found = getDataStore().getAccount(accountId);
        return found.map(Response::ok)
                .map(Response.ResponseBuilder::build)
                .orElseGet(() -> Response.status(NOT_FOUND)
                        .entity("No such account")
                        .build());
    }


}
