package io.gitlab.strwrite.accountant;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

@Slf4j
public class AccountantApplication {
    public static void main(String[] args) throws Exception {

        Server server = new Server(8080);

        ServletContextHandler ctx =
                new ServletContextHandler(ServletContextHandler.NO_SESSIONS);

        ctx.setContextPath("/");
        server.setHandler(ctx);

        ServletHolder serHol = ctx.addServlet(ServletContainer.class, "/*");
        serHol.setInitOrder(1);
        serHol.setInitParameter(
                "jersey.config.server.provider.packages",
                "io.gitlab.strwrite.accountant.resources");

        try {
            server.start();
            server.join();
        } catch (Exception ex) {
            log.error("Failed to start", ex);
        } finally {

            server.destroy();
        }
    }
}
