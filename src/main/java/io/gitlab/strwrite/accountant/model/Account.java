package io.gitlab.strwrite.accountant.model;

import io.gitlab.strwrite.accountant.model.requests.AccountCreateRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    public Account(AccountCreateRequest acr) {
        id = UUID.randomUUID().toString();
        ownerName = acr.getOwnerName();
        amount = acr.getAmount();
        if (amount == null) {
            amount = new BigInteger("0");
        }
        transfers = new ArrayList<>();
    }

    public Account(Account account) {
        id = account.getId();
        ownerName = account.getOwnerName();
        transfers = new ArrayList<>(account.getTransfers());
        amount = account.getAmount();
        freezed = account.getFreezed();
    }

    private String id;
    private String ownerName;
    private List<String> transfers;
    private BigInteger amount;
    private BigInteger freezed = new BigInteger("0");
}
