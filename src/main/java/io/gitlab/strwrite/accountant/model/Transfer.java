package io.gitlab.strwrite.accountant.model;

import io.gitlab.strwrite.accountant.model.requests.TransferRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transfer {
    public Transfer(TransferRequest req) {
        this.from = req.getFrom();
        this.to = req.getTo();
        this.id = UUID.randomUUID().toString();
        this.amount = req.getAmount();
    }

    private Account fromAccount;
    private Account toAccount;
    private String id;
    private String from;
    private String to;
    private BigInteger amount;
}
