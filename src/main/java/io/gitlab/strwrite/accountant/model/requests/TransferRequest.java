package io.gitlab.strwrite.accountant.model.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransferRequest {
    private String from;
    private String to;
    private BigInteger amount;
}
