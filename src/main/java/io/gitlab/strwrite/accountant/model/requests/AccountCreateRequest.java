package io.gitlab.strwrite.accountant.model.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreateRequest {
    private String ownerName;
    private BigInteger amount;
}
