#!/usr/bin/env bash

function accounts() {
    echo "Accounts:"
    curl "localhost:8080/accounts" | python -m json.tool
}

function petyacreate(){
    PETYA_LOCATION=$(
        cat << EOF | curl -f -d @- -si -H "Content-Type: application/json" "localhost:8080/accounts" | grep -oP 'Location: \K.*'
        {
            "ownerName": "Petya",
            "amount": "20"
        }
EOF
)
    PETYA_ID=$(curl "$(printf "${PETYA_LOCATION}" | tr -d '\n\r')" | python -c '
import sys, json
res = json.load(sys.stdin)
print(res["id"])
    ')

}

function vasyacreate() {
    VASYA_LOCATION=$(
        cat << EOF | curl -f -d @- -si -H "Content-Type: application/json" "localhost:8080/accounts" | grep -oP 'Location: \K.*'
        {
            "ownerName": "Vasya",
            "amount": "0"
        }
EOF
)
    VASYA_ID=$(curl "$(printf "${VASYA_LOCATION}" | tr -d '\n\r')" | python -c '
import sys, json
res = json.load(sys.stdin)
print(res["id"])
    ')
}

function transfer() {
    cat << EOF | curl -d @- -H "Content-Type: application/json" "localhost:8080/transfers"
    {
        "amount": $3,
        "from": "${1}",
        "to": "${2}"
    }
EOF
}

if [[ "$1" == "manual" ]] ; then
    echo "
use
    vasyacreate
to create account for Vasya with 0 amount on account"
    echo "
use
    petyacreate
to create account for Petya with 20 amount on account"
    echo "
use
    accounts
to see accounts"
    echo '
use
    transfer ${PETYA_ID} ${VASYA_ID} 5
to transfer 5 amount from Petya to Vasya'
else
    vasyacreate
    petyacreate
    accounts
    transfer ${PETYA_ID} ${VASYA_ID} 5
    accounts
fi

