# Accountant Microsevice

This is an example how to use Jetty + Jersey and Apache Ignite to build an accountant - simple application with REST API to support accounts processing.

Service does not have any persistence by default - datastore runs completely in memory.

## REST

Supported endpoinst are following:

### `GET` /accounts

Request all available accounts 

### `POST` /accounts

Create new account, example:

``` json
{
    "ownerName": "Vasya",
    "amount": 0    
}
```

 - `ownerName` is a name of owner of account, not used programmatically, introduced only for convenience
 - `amount` is an amount of some resource (f.e. money) on account 

Endpoint returns `Location` header targeted at the following endpoint:

### `GET` /accounts/`{accountId}`

Get one account specified by `{accountId}`. Return example:

``` json
{
    "id": "9ea67a6d-b9a4-4a4e-b361-7200d1b4bd3f",
    "ownerName": "Vasya",
    "transfers": [],
    "amount": 0,
    "freezed": 0
}
```

 - `id` - identifies account
 - `ownerName` - name of owner, specified when account has been created
 - `transfers` - list with identifiers of transfers, happened with this account
 - `amount` - amount of resource (f.e. money) stored on account currently
 - `freezed` - amount of resource (f.e. money) freezed on account, before they could be fully transfered to receiver

### `POST` /transfers

Transfer some amount from one account to another.

Example body:

``` json
    {
        "amount": 10,
        "from": "9ea67a6d-b9a4-4a4e-b361-7200d1b4bd3f",
        "to": "75b66937-401d-4ebc-89e2-59c0ea36b72d"
    }
```

 - `amount` - amount of money to transfer
 - `from` - identifier of account where amount should be frozen and then transfered from
 - `to` - identifier of account where resource should be transferred to

## Build And Run

### Requirements

 - git
 - maven 3
 - bash
 - python 2.7 or 3

### Process

Just run following in your console:

```
git clone https://gitlab.com/str.write/accountant
cd accountant
chmod +x ./build_run.sh
chmod +x ./client.sh
./build_run.sh manual
```

After some time, server would be up and script output would be:

```
---------------------
Server started
Press any key to complete

```

Don't press  anything yet. 

Open other console, navigate in same directory and run following:

```
source client.sh manual
```

You should see following output with a bunch of examples

```
use
    vasyacreate
to create account for Vasya with 0 amount on account

use
    petyacreate
to create account for Petya with 20 amount on account

use
    accounts
to see accounts

use
    transfer ${PETYA_ID} ${VASYA_ID} 5
to transfer 5 amount from Petya to Vasya

```

Now check how many accounts we have

```
$ accounts
Accounts:
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    15  100    15    0     0      2      0  0:00:07  0:00:05  0:00:02     3
{
    "accounts": []
}
```

Create accounts for Vasya and Petya

```
$ petyacreate
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   104  100   104    0     0   2212      0 --:--:-- --:--:-- --:--:--  2212

$ vasyacreate
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   103  100   103    0     0   3322      0 --:--:-- --:--:-- --:--:--  6437

```

Check their accounts

```
$ accounts
Accounts:
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   223  100   223    0     0  13937      0 --:--:-- --:--:-- --:--:-- 13937
{
    "accounts": [
        {
            "id": "75b66937-401d-4ebc-89e2-59c0ea36b72d",
            "ownerName": "Petya",
            "transfers": [],
            "amount": 20,
            "freezed": 0
        },
        {
            "id": "9ea67a6d-b9a4-4a4e-b361-7200d1b4bd3f",
            "ownerName": "Vasya",
            "transfers": [],
            "amount": 0,
            "freezed": 0
        }
    ]
}
```

Transfer from Petya to Vasya

```
$ transfer ${PETYA_ID} ${VASYA_ID} 5
```

Check how amounts has changed

```
$ accounts
Accounts:
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   299  100   299    0     0   9343      0 --:--:-- --:--:-- --:--:-- 18687
{
    "accounts": [
        {
            "id": "75b66937-401d-4ebc-89e2-59c0ea36b72d",
            "ownerName": "Petya",
            "transfers": [
                "fd221e24-8c5e-4f66-b1c2-74b7f1b41264"
            ],
            "amount": 15,
            "freezed": 0
        },
        {
            "id": "9ea67a6d-b9a4-4a4e-b361-7200d1b4bd3f",
            "ownerName": "Vasya",
            "transfers": [
                "fd221e24-8c5e-4f66-b1c2-74b7f1b41264"
            ],
            "amount": 5,
            "freezed": 0
        }
    ]
}

```

Now you can press Enter in the first console to stop the service. 


## Published

Check [list of tags](https://gitlab.com/str.write/accountant/tags/1.0.0) to see all published versions.

Published jar is a standalone executable file, you can run it with `java -jar`:

``` bash
java -jar str-accountant.jar
```



