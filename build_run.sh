#!/usr/bin/env bash

mvn clean package || exit 1
java -jar target/str-accountant.jar &
SERVICE_PID=$!
echo ${SERVICE_PID}

c=0
while ((c++ <100)) ; do
    sleep 1
    if curl localhost:8080 ; then break ; fi
done

echo -e "\n\n\n\n\n\n\n---------------------"
echo "Server started"

if [ "$1" == "manual" ] ; then
    read -p "Press any key to complete" -n1 -s
else
    . ./client.sh
fi

echo "Killing server ${SERVICE_PID}"
kill ${SERVICE_PID} && echo "server killed"




